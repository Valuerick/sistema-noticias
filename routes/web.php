<?php

Route::get('/', function () {
    return view('home.index');
});

Route::get('/tecnologia', function () {
    return view('noticias.index');
});

Route::get('/tecnologia/titulo-noticia', function () {
    return view('noticias.visualizar');
});

Route::get('/contato', function (){
    return view('home.contato');
});

Route::get('/admin/home',function (){
    return view('admin.home.index');
});

Route::get('/admin/noticias',function (){
    return view('admin.noticias.index');
});
